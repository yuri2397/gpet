<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\User;
use App\Models\Professor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfesseurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(auth()->id());
        if ($user->hasRole("super admin")) {
            return Professor::with('account')->orderBy('created_at', 'desc')->get();
        }
        return Professor::whereDerpartementId($user->departement_id)->orderBy('created_at', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:professors,email',
            'status' => 'required',
            'phone_number' => 'required',
            'account_number' => 'required',
            'departement_id' => 'required|exists:departements,id',
            'rip' => 'required',
            'key' => 'required',
            'bank_id' => 'required|exists:banks,id'
        ]);

        $prof = new Professor;
        $prof->registration_number = $this->randomInt('professors', 'registration_number');
        $prof->first_name = $request->first_name;
        $prof->last_name = $request->last_name;
        $prof->email = $request->email;
        $prof->status = $request->status;
        $prof->departement_id = $request->departement_id;
        $prof->phone_number = $request->phone_number;
        $prof->job = $request->job ?? null;
        $prof->save();

        $compte = new Account();
        $compte->account_number = $request->account_number;
        $compte->rip = $request->rip;
        $compte->key = $request->key;
        $compte->bank_id = $request->bank_id;
        $compte->professor_id = $prof->id;
        $compte->save();

        return response()->json($this->show($prof->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prof = Professor::with('account')
        ->with('departement')
        ->with('account.bank')
        ->find($id);

        $user = User::find(auth()->id());
        if($user->hasRole("super admin")){
            $prof->courses;
            $prof->coursesDo = $prof->coursesDo()
            ->whereYear('courses_has_professors.date', date('Y'))
            ->whereMonth('courses_has_professors.date', date('n') - 1)
            ->join('courses', 'courses_has_professors.course_id', 'courses.id')
            ->where('courses.departement_id', $prof->departement_id)
            ->select(
                "courses_has_professors.course_id",
                "courses_has_professors.professor_id",
                "courses_has_professors.amount",
                DB::raw('SUM(courses_has_professors.hours) as total_hours'),
                DB::raw(' courses_has_professors.amount * SUM(courses_has_professors.hours) as total_sales')
            )
            ->groupBy(
                'courses_has_professors.course_id',
                "courses_has_professors.amount",
                "courses_has_professors.professor_id",
            )
            ->get();
        }
        else{
            $prof->courses = $prof->courses()->whereDepartementId($prof->departement_id)->get();
            $prof->coursesDo = $prof->coursesDo()
            ->whereYear('courses_has_professors.date', date('Y'))
            ->whereMonth('courses_has_professors.date', date('n') - 1)
            ->join('courses', 'courses_has_professors.course_id', 'courses.id')
            ->where('courses.departement_id', $prof->departement_id)
            ->select(
                "courses_has_professors.course_id",
                "courses_has_professors.professor_id",
                "courses_has_professors.amount",
                DB::raw('SUM(courses_has_professors.hours) as total_hours'),
                DB::raw(' courses_has_professors.amount * SUM(courses_has_professors.hours) as total_sales')
            )
            ->groupBy(
                'courses_has_professors.course_id',
                "courses_has_professors.amount",
                "courses_has_professors.professor_id",
            )
            ->get();
        }

        return $prof;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|exists:professors,email',
            'status' => 'required',
            'phone_number' => 'required',
            'account_number' => 'required',
            "departement_id" => "required|exists:departements,id",
            'rip' => 'required',
            'key' => 'required',
            'bank_id' => 'required|exists:banks,id'
        ]);

        Professor::whereId($id)->update([
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "status" => $request->status,
            "phone_number" => $request->phone_number,
            "job" => $request->job,
            "departement_id" => $request->departement_id,
        ]);

        Account::whereProfessorId($id)->update([
            "account_number" => $request->account_number,
            "rip" => $request->rip,
            "key" => $request->key,
            "bank_id" => $request->bank_id,
        ]);

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(DB::table("professors")->whereId($id)->delete(), 200);
    }

    public function desableAccount(Request $request, $id)
    {
        $request->validate(['is_active' => 'required|boolean']);
        $prof  = Professor::find($id);
        $prof->is_active = $request->is_active;
        $prof->save();
        return $this->show($id);
    }

    public function search($data)
    {
        $user = User::find(auth()->id());
        if ($user->hasRole("super admin")) {
            return Professor::where("first_name", 'like', '%' . $data . '%')
                ->orWhere('last_name', 'like', '%' . $data . '%')
                ->orWhere('registration_number', 'like', '%' . $data . '%')
                ->orderBy('created_at', 'desc')->get();
        }
        return Professor::whereDerpartementId($user->departement_id)->orderBy('created_at', 'desc')->get();
    }
}
