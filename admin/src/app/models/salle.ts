export class Salle {
  id!: number;
  number!: number;
  name!: string;
  capacity!: number;
  batiment_id!: number;
  departement_id!: number;
  batiment!: string;
  departement!: string;
}
